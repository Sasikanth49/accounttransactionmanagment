package com.accountdetails.accounttransaction.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accountdetails.accounttransaction.entity.Account;

public interface AccountRepo extends JpaRepository<Account, Integer> {

}
