package com.accountdetails.accounttransaction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.accountdetails.accounttransaction.entity.Account;
import com.accountdetails.accounttransaction.repo.AccounttransactionRepo;
import com.accountdetails.accounttransaction.vo.Response;

@RestController
public class AccountController {

	@Autowired
	AccounttransactionRepo accounttransactionRepo;

	@GetMapping("/transfer")
	public @ResponseBody Response transaction(@RequestParam int sourceAccount, @RequestParam int destinationAccount,@RequestParam int transacationAmount) {
		Response response= new Response("", sourceAccount+"", destinationAccount+"",transacationAmount+"", "No Error");
		if(transacationAmount <=0) {
			response.setMessage("Transacation Amount should be more than 1 ruppee");
		} else 		
		if(accounttransactionRepo.exists(sourceAccount) && accounttransactionRepo.exists(destinationAccount)) {
			
		
		Account saccounts = accounttransactionRepo.findOne(sourceAccount);
		Account daccounts = accounttransactionRepo.findOne(destinationAccount);
		int saccountbalance = saccounts.getAccountbalance();

		if(saccountbalance < transacationAmount) {
			response.setErrorMessage("Insufficent Amount...");
		} else {
			saccountbalance = saccountbalance - transacationAmount;
			int daccountbalance = daccounts.getAccountbalance();
			daccountbalance = daccountbalance + transacationAmount;
			saccounts.setAccountbalance(saccountbalance);
			daccounts.setAccountbalance(daccountbalance);
			accounttransactionRepo.save(saccounts);
			accounttransactionRepo.save(daccounts);

			response.setMessage("Transaction Successfull");

		}
		} else {
			response .setMessage("one of the account Number is Invalid "); 
		}

		return response;
	}



}
