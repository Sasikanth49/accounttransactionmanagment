package com.accountdetails.accounttransaction.vo;

public class Response {

	private String message;
	private String sAccountNumber;
	private String dAccountNumber;
	private String transacationAmount;
	private String errorMessage;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getsAccountNumber() {
		return sAccountNumber;
	}
	public void setsAccountNumber(String sAccountNumber) {
		this.sAccountNumber = sAccountNumber;
	}
	public String getdAccountNumber() {
		return dAccountNumber;
	}
	public void setdAccountNumber(String dAccountNumber) {
		this.dAccountNumber = dAccountNumber;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public Response(String message, String sAccountNumber, String dAccountNumber, String transacationAmount,
			String errorMessage) {
		super();
		this.message = message;
		this.sAccountNumber = sAccountNumber;
		this.dAccountNumber = dAccountNumber;
		this.transacationAmount = transacationAmount;
		this.errorMessage = errorMessage;
	}
	public String getTransacationAmount() {
		return transacationAmount;
	}
	public void setTransacationAmount(String transacationAmount) {
		this.transacationAmount = transacationAmount;
	}
	
	
}


